var express = require('express');
app = express()

app2 = express()
app2.get('/', (req, res) => res.send('server2'))
app2.get('/opa', (req, res) => res.send('opa server2'))

app3 = express()
app3.get('/', (req, res) => res.send('server3'))
app3.get('/opa', (req, res) => res.send('opa server3'))

const vhost = (hostname, app) => (req, res, next) => {
    const host = String(req.headers.host)

    if(host.includes(hostname)){
        return app(req, res, next)
    }else{
        next()
    }
}

app.use(vhost('server2', app2))
app.use(vhost('server3', app3))

app.get('/', (req, res) => res.send('aplicacao principal'))
app.get('/opa', (req, res) =>  res.send('opa server principal'))
app.get('*', (req, res) => res.status(404).send('404 - Ops, nao encontrado'))


app.listen(80, () => console.log('aplicacao rodando'));